# Da Mail Client (dmc)

Simple mail client for the terminal.


## install

Just copy/symlink the script `dmc.py` to a directory part of the PATH
environment variable to execute it when typed at any location.

```bash
cp dmc.py ~/.local/bin/dmc
ln -s $( pwd )/dmc.py ~/.local/bin/dmc
```

configuration: `cp config.example ~/.dmc/config`  
accounts     : `cp accounts.example ~/.dmc/accounts`


## usage

```bash
$ dmc -h

usage: dmc [shorthand] [-h] [OPTION..] [folder] [{msgid|search..}]

  simple command-line based mail client.
  internet access is required.

  mutt sucks less, dmc sucks more :)

options:
  -d      set debug level (supply multiple to increase)
  -f      list mail folders and exit
  -h      show this help message and exit
  -l      list account(s) and exit
  -p      use less-pager (if available)
  -a ID   set active account to work with (see -l)
          ID can be a number or portion of an address
  -m num  show number of messages
  -H      prefer html over plain, if available

shorthands:
  d       delete message (default latest)
  l       view latest message
  t       toggle seen/unseen message (default latest)
  u       list unseen messages

optional:
  folder  folder to use (default: inbox)
  msgid   message id from message list
  search  search in message headers

quick reference:
  accounts dmc -l
  active   dmc -a 1
  messages dmc [folder]
  view     dmc [folder] 1
  search   dmc [folder] subject:invoice #1000
           dmc [folder] addr:some@one

text/html messages:
  dmc prefers plain text over html, if a message contains both,
  use -H to force output of the html part of the message.
  to parse html with an external application (e.g. elinks),
  either set "html_cmd" in ~/.dmc/config or pipe stderr (e.g):
  dmc <msgid> 2> >(elinks --force-html --dump)

configuration files:
  ~/.dmc/config
  ~/.dmc/accounts
  ~/.dmc/account-id

See README.md for Full documentation.
```


## setup

First: take a look at `accounts.example`; copy it to `~/.dmc/accounts` and
add at least one account and make it accesible only for your login (dmc will
notify you if it's not secure enough).

Second: (not required) copy `config.example` to `~/.dmc/config` and adjust the
items to whatever you see fit (read the comments).


## basic usage

```bash
dmc -l
dmc -a 1
dmc
dmc 1
dmc sent
dmc sent 1
```


## shorthand actions

| shorthand | action |
| --- | --- |
| d | delete message (default latest) |
| l | view latest message |
| t | toggle seen/unseen message (default last) |
| u | list unseen messages |

```bash
dmc l
dmc t
```


## search

```bash
dmc [folder] <search key:word(s)>
```

* folder is optional

* the colon (:) denotes a search key; dmc tries to gather space-separated
  stuff, though quoting doesn't hurt :)

* subsequent search keys are read like "and also".

* accepted search keys (see [rfc3501:SEARCH Command](
https://tools.ietf.org/html/rfc3501#page-50)):

```
bcc before body cc from keyword larger not on sentbefore
senton sentsince since smaller subject text to uid unkeyword
```

* special search key "addr" is a abbreviation for "to, from, cc"

* dates must be formatted as "\<day\>-\<3-letter-month\>-\<fullyear\>".

```bash
dmc addr:some@one
dmc sent addr:some@one
dmc inbox subject:how are you addr:some@one
dmc sentsince:1-jan-1970
dmc addr:some subject:re addr:one since:1-jan-1970
dmc sent subject:hi before:2-feb-1970
dmc body:installed dmc, nice sentsince:1-jan-1970
```
