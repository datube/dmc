#!/usr/bin/env python3

# Copyright (C) <YEAR(S)> <AUTHOR>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
#    On Debian systems, the complete text of the GNU General
#    Public License can be found in `/usr/share/common-licenses/GPL-3'.


# import required modules  ---------------------------------------------------
import os               # Miscellaneous operating system interfaces
import sys              # System-specific parameters and functions
import argparse         # Parser for command-line options
import configparser     # Configuration file parser
import base64           # Data Encoding
import imaplib          # IMAP4 protocol client
import email            # An email and MIME handling package
import ssl              # TLS/SSL wrapper for socket objects
from datetime import datetime # Basic date and time type
import locale           # Internationalization services
import re               # Support for regular expressions


# Classes ====================================================================
class Config(): #-------------------------------------------------------------

  def __init__(self):
    # what's known about "this" script
    self.script     = os.path.abspath(__file__)
    self.basename   = os.path.basename(self.script)
    self.dirname    = os.path.dirname(self.script)
    self.cwd        = os.getcwd()
    self.args       = None

    # internally used
    self.accounts = {}
    self.locale      = self._locale()
    self.fn_accounts = self._path('accounts')
    self.fn_config   = self._path('config')
    self.fn_active   = self._path('account-id')
    self.r_folder    = "inbox"
    self.r_msgid     = None
    self.r_search    = []
    self.r_search_token = ':'
    self.proc_pager  = None
    self.r_shorthand = None

    # options (either argument or config file)
    self.opt_account_id = None
    self.opt_date_format = "%a, %x %X"
    self.opt_address_length = 20
    self.opt_subject_length = 30
    self.opt_console_width = 79
    self.opt_console_height = 10
    self.opt_messages = self.opt_console_height
    self.opt_display_headers = "date to cc from reply-to subject"
    self.opt_attachment_info = "simple"
    self.opt_auto_mark = False
    self.opt_use_pager = False
    self.opt_debug = None
    self.opt_list_folders = False
    self.opt_html_cmd = None
    self.opt_html_charset = 'utf-8'
    #
    self.console_size()

  def __repr__(self):
    return """Config:
    self.opt_account_id = {self.opt_account_id}
    self.opt_date_format = {self.opt_date_format}
    self.opt_address_length = {self.opt_address_length}
    self.opt_subject_length = {self.opt_subject_length}
    self.opt_console_width = {self.opt_console_width}
    self.opt_console_height = {self.opt_console_height}
    self.opt_messages = {self.opt_messages}
    self.opt_display_headers = {self.opt_display_headers}
    self.opt_attachment_info = {self.opt_attachment_info}
    self.opt_auto_mark = {self.opt_auto_mark}
    """.format(self=c).strip()

  def _locale(self):
    locale = "C"
    if 'LC_TIME' in os.environ:
      locale = os.environ['LC_TIME']
    elif 'LANG' in os.environ:
      locale = os.environ['LANG']
    return locale

  def console_size(self):
    cr = None
    try:
      import fcntl, termios, struct, os
      cr = struct.unpack('hh', fcntl.ioctl(1, termios.TIOCGWINSZ,'1234'))
    except:
      pass
    if cr:
      self.opt_console_width = cr[1]
      self.opt_console_height = cr[0]-2

  def _path(self, fn):
    conf_dir = "{}".format(
        os.path.join(
          os.path.expanduser('~'),
          ".{}".format(
            os.path.splitext(self.basename)[0]
            ),
          ))
    return "{}".format(os.path.join(conf_dir, fn))


class Account(): #------------------------------------------------------------

  def __init__(self, s=None):
    self.addr = None
    self.host = None
    self.user = None
    self.pwrd = None
    self.id   = 0
    try:
      self.addr = s.split(',')[0]
      self.host = s.split(',')[1]
      up = decode(base64.b64decode(s.split(',')[2])).split("\x00")
      self.user = up[1]
      self.pwrd = up[2]
    except:
      pass


class Message(): #------------------------------------------------------------

  def __init__(self):
    self.__message = None
    self.__body = {}
    self.__headers = {}
    self.email_localize_date = None

  def message_from(self, raw_message):
    if hasattr(raw_message, 'decode'):
      self.__message = email.message_from_bytes(raw_message)
    else:
      self.__message = email.message_from_string(raw_message)
    if self.__message:
      self.__compile()

  def __compile(self):
    # group (separated) message headers
    for _h, _v in self.__message.items():
      _v = email.header.decode_header(_v)
      if _v:
        _v, charset = _v[0]
        try:
          _v = _v(charset) if charset else _v
        except:
          _v = _v.decode('utf-8')
        _v = [v for v in _v.strip().replace("\r", "").split("\n")]
        _v = [v.strip() for v in _v if v.strip()]
        if not _h in self.__headers:
          self.__headers[_h] = []
        self.__headers[_h].append(_v)
    for part in self.__message.walk():
      if part.get_content_maintype() == 'text' and \
          part.get_content_subtype() in ['html','plain']:
        body = part.get_payload(decode=True)
        charset = part.get_content_charset()
        self.__body[part.get_content_subtype()] = \
            body.decode('utf-8' if charset == None else charset)
    # body mimetype(s) as "header"
    self.__headers['Mimetype'] = [[
        ", ".join(['text/{}'.format(k) for k in self.__body])]]

  def headers(self, headers="*"):
    ret = []
    # listify (requested) headers
    headers = headers.lower().split()
    # (formatted) requested (in order) headers
    for _h in headers:
      if _h == "*":
        _h = [h for h in self.__headers \
            if (not h.lower() in headers or len(headers) == 1)]
      else:
        _h = [h for h in self.__headers if h.lower() == _h]
      for _h in _h:
        for _v in self.__headers[_h]:
          _v = ["  "*(i > 0)+v for i, v in enumerate(_v)]
          ret.append(_h+": "+os.linesep.join(_v))
    return os.linesep.join(ret)

  def walk(self, parts=None):
    ret = None
    if self.__message:
      ret = self.__message.walk()
    return ret

  def set(self, p=()):
    if self.__headers:
      _h, _v = p
      self.__headers[_h] = [[_v]]

  def get(self, header=""):
    ret = ""
    if header in self.__headers:
      ret = " ".join([" ".join(v) for v in self.__headers[header]])
    return ret

  def body(self):
    return self.__body

  def attachments(self, info=None):
    ret = []
    if self.__message:
      for part in self.__message.walk():
        fileName = part.get_filename()
        if part.get_content_maintype() == 'multipart' or \
            part.get('Content-Disposition') is None:
          continue

        if info:
          filename    = part.get_filename()
          contenttype = part.get_content_type()
          attachment  = part.get('Content-Disposition')
          ret.append((filename, contenttype, attachment))

      # return attachment list
      if info:
        ret = sorted(ret, key=lambda x:x[1])
        if info == "full":
          ret = os.linesep.join([ "Attachment: {}".format(attachment)
              for filename, contenttype, attachment in ret ])
          return ret
        else:
          ret = [ '"{}" ({})'.format(filename, contenttype)
              for filename, contenttype, attachment in ret ]
          if ret:
            ret = "Attachments: {}".format(', '.join(ret))
          return ret


class imapclient(): #---------------------------------------------------------
  """https://pymotw.com/2/imaplib"""

  def __init__(self):
    self.imap = None
    self._debug = 0
    self.readonly = True
    self._select = None
    self.__re_list = re.compile(
        r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')

  def decode(self, s):
    return (s.decode('utf-8') if hasattr(s,'decode') else s)

  def connect(self, host, usr, pwd):
    imap = None
    try:
      imap = imaplib.IMAP4_SSL(host=host)
      imap.login(usr, pwd)
    except:
      pass
    if imap:
      imaplib.Debug = self._debug
      self.imap = imap
    return self.imap != None

  def isconnected(self):
    return self.imap != None

  def disconnect(self):
    if self.imap:
      if self._select:
        self.imap.close()
      self.imap.logout()
    self.imap = None

  def debug(self, lvl = None):
    if lvl:
      if str(lvl).isdigit():
        lvl = int(lvl)
        self._debug = lvl
        imaplib.Debug = self._debug
    else:
      return imaplib.Debug

  def select(self, folder = None):
    ret=''
    if self.imap:
      if folder:
        if self._select:
          self.imap.close()
        self._select = None
        try:
          # seems that folders need to be capitalized (some do, some don't)
          folder = folder.capitalize()
          rc, data = self.imap.select(folder, readonly = self.readonly)
          if rc == "OK":
            self._select = folder
            ret = self.decode(data[0])
        except:
          pass
      else:
        self._select = None
    else:
      self._select = None
    return ret

  def store(self, msgset, cmd, flags):
    rc = ''
    if self._select:
      try:
        rc, data = self.imap.store(msgset, cmd, flags)
      except:
        pass
    return rc == "OK"

  def expunge(self):
    ret = False
    if self._select:
      try:
        self.imap.expunge()
        ret = True
      except:
        pass
    return ret

  def status(self):
    ret = "none"
    if self._select:
      try:
        rt, data = self.imap.status(self._select, '(MESSAGES RECENT UNSEEN)')
        if rt == "OK":
          ret = data[0]
      except:
        pass
    return ret

  def list(self, folder = ''):
    ret=[]
    if self.imap:
      if folder.find('"') < 0:
        folder = '"{}"'.format(folder)
      rc, data = self.imap.list(directory=folder)
      if rc == "OK":
        for line in data:
          line = self.decode(line)
          flags, delim, folder = self.__list_response_parser(line)
          ret.append((folder, flags))
    return ret

  def __list_response_parser(self, line):
    flags, delim, folder = self.__re_list.match(line).groups()
    folder = folder.strip('"').lower()
    flags = flags.replace('\\','').lower()
    return (flags, delim, folder)

  def search(self, crit="ALL"):
    ret = []
    if self.imap and self._select:
      try:
        rc, data = self.imap.search(None, crit)
        if rc == "OK":
          ret = sorted(self.decode(data[0]).split(), key=int, reverse=True)
      except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        sys.stderr.write('error: {}'.format(exc_value))
    return ret

  def fetch(self, msgid=None, full=False):
    ret = None
    flags = ""
    raw_message = None
    if msgid:
      msgid = str(msgid)
      if self.imap and self._select:
        q = "RFC822" if full else "BODY.PEEK[HEADER]"
        #rc, data = self.imap.fetch(str(msgid), '(FLAGS BODY.PEEK[HEADER] BODYSTRUCTURE BODY.PEEK[1])'.format(q))
        rc, data = self.imap.fetch(msgid, '(FLAGS {})'.format(q))
        if rc == "OK":
          flags, raw_message  = data[0]
      if raw_message:
        flags = imaplib.ParseFlags(flags)
        flags = ' '.join([f.decode().replace('\\','') for f in flags])
        ret = Message()
        ret.message_from(raw_message)
        ret.set(('Flags', flags))
        ret.set(('Msgid', "{}/{}".format(self._select, msgid)))
    return ret


# Functions ==================================================================
def parse_arguments(): #------------------------------------------------------
  """Reads the given command-line arguments"""

  usage = "{c.basename} [shorthand] [-h] [OPTION..] [folder] "+\
          "[{{msgid|search..}}]"
  usage = usage.format(c=c)

  description = """
    simple command-line based mail client.
    internet access is required.
    \0
    mutt sucks less, {c.basename} sucks more :)
  """.format(c=c).strip()

  epilog = """
    shorthands:
    \0  d       delete message (default latest)
    \0  m       view latest or first unseen message
    \0  t       toggle seen/unseen message (default latest)
    \0  u       list unseen messages
    \0
    optional:
    \0  folder  folder to use (default: inbox)
    \0  msgid   message id from message list
    \0  search  search in message headers
    \0
    \0quick reference:
    \0  accounts {c.basename} -l
    \0  active   {c.basename} -a 1
    \0  messages {c.basename} [folder]
    \0  view     {c.basename} [folder] 1
    \0  search   {c.basename} [folder] subject:invoice #1000
    \0           {c.basename} [folder] addr:some@one
    \0
    \0text/html messages:
    \0  {c.basename} prefers plain text over html, if a message contains both,
    \0  use -H to force output of the html part of the message.
    \0  to parse html with an external application (e.g. elinks),
    \0  either set "html_cmd" in {fn_config} or pipe stderr (e.g):
    \0  {c.basename} <msgid> 2> >(elinks --force-html --dump)
    \0
    \0configuration files:
    \0  {fn_config}
    \0  {fn_accounts}
    \0  {fn_active}
    \0
    See README.md for Full documentation.
   """.format(c=c,
        fn_config=shrinkuser(c.fn_config),
        fn_accounts = shrinkuser(c.fn_accounts),
        fn_active = shrinkuser(c.fn_active),
      )

  parser = argparse.ArgumentParser(
    add_help = False,
    formatter_class=argparse.RawTextHelpFormatter,
    usage = usage,
    description = os.linesep.join(
      [ "  {}".format(l.strip()).replace("\0",'')
        for l in description.splitlines() if l.strip()
      ]),
    epilog = os.linesep.join(
      [ l.strip().replace("\0",'')
        for l in epilog.splitlines() if l.strip()
      ]),
  )

  parser._optionals.title = "options"
  #parser._positionals.title ="positional arguments"

  parser.add_argument('-a', dest="account_id", metavar="ID", type=str,
                      help="set active account to work with (see -l)"+
                      "\nID can be a number or portion of an address",
                      action="store", default=None)
  parser.add_argument('-d', dest="debug",
                      help="set debug level (supply multiple to increase)",
                      action="count", default=2)
  parser.add_argument('-f', dest="list_folders",
                      help="list mail folders and exit",
                      action="store_true", default=False)
  parser.add_argument('-h', dest="help",
                      help="show this help message and exit",
                      action="store_true", default=False)
  parser.add_argument('-l', dest="list_accounts",
                      help="list account(s) and exit",
                      action="store_true", default=False)
  parser.add_argument('-n', dest="use_pager",
                      help="do not use less-pager",
                      action="store_false", default=None)
  parser.add_argument('-m', dest="messages", type=int, metavar="num",
                      help="show number of messages",
                      action="store", default=None)
  parser.add_argument('-H', dest="prefer_html",
                      help="prefer html over plain, if available",
                      action="store_true", default=False)

  parser.add_argument('remainders', help=argparse.SUPPRESS,
                      nargs=argparse.REMAINDER)

  args = parser.parse_args()

  if args.help:
    parser.print_help()
    sys.exit(0)

  # use whatever value they got
  for arg in ['account_id', 'list_accounts', 'prefer_html']:
    if hasattr(args, arg):
      setattr(c, 'opt_{}'.format(arg), getattr(args, arg))
      delattr(args, arg)

  # use only when set (override from config)
  for arg in ['messages', 'use_pager', 'debug', 'list_folders']:
    if hasattr(args, arg):
      if getattr(args, arg) != None:
        setattr(c, 'opt_{}'.format(arg), getattr(args, arg))
        delattr(args, arg)

  # parse remainders
  (c.r_shorthand, c.r_folder, c.r_msgid, c.r_search) = \
      parse_arguments_remains(args.remainders)
  delattr(args, 'remainders')

  c.args = args # all that's left behind


def parse_arguments_remains(r_): #--------------------------------------------
  r_folder = None
  r_msgid = None
  r_search = None
  r_shorthand = None

  # parse shorthand actions (must be first single char)
  idx_shorthand = [ i for i,r in enumerate(r_) if len(r) == 1 and i == 0 ]
  if idx_shorthand:
    idx_shorthand = idx_shorthand[0]
    r_shorthand = r_[idx_shorthand]
    r_.pop(idx_shorthand)

  for i,r in enumerate(r_):
    f_search_token = (r.find(c.r_search_token) > -1)
    if i == 0 and not (r.isdigit() or f_search_token):
      r_folder = r
    elif r.isdigit() and (i==0 or (r_folder and i==1)):
      r_msgid = r
    else:
      if not r_search and f_search_token:
        r_search = [r]
      elif r_search:
        r_search.append(r)
      else:
        sys.stderr.write("warn: illegal `{}'\n".format(r))

  # apply defaults if required
  r_folder = (r_folder if r_folder else c.r_folder)
  r_msgid  = (r_msgid  if r_msgid  else c.r_msgid)
  r_search = (r_search if r_search else c.r_search)
  return r_shorthand, r_folder, r_msgid, r_search


def parse_search_arguments(search_args): #------------------------------------
  ret = None
  r_search = []
  r_addr = []
  r_search_invalid = []

  r_search_allowed_months = [
      "jan", "feb", "mar", "apr", "may", "jun",
      "jul", "aug", "sep", "oct", "nov", "dec"]

  allowed_search_keys = """
    bcc body cc from keyword larger not smaller subject text to uid unkeyword
  """.strip().split()
  allowed_search_keys_date = """
    before on sentbefore senton sentsince since
  """.strip().split()

  # gather multi part items (like: subject:this that)
  for r in (' '.join(search_args)).split(' '):
    if r.find(c.r_search_token) > -1:
      if r.startswith('addr{}'.format(c.r_search_token)): # special "addr"
        r = r.split(c.r_search_token)
        for f in ['to','from','cc']:
          r_addr.append(['{}{}{}'.format(f, c.r_search_token, r[1])])
      else:
        search_key = r.split(c.r_search_token)[0]
        if search_key.lower() in allowed_search_keys:
          r_search.append([r])
        elif search_key.lower() in allowed_search_keys_date:
          # basic date validation (only format)
          date = r.split(c.r_search_token)[1].split('-')
          if len(date) != 3:
            r_search_invalid_dates.append(r.split(c.r_search_token)[1])
          elif not ( date[0].isdigit() and date[2].isdigit() and \
                (date[1].lower() in r_search_allowed_months) ):
              r_search_invalid.append(r)
          else:
            r_search.append([r])
        else:
          r_search_invalid.append(r)
    else:
      # append it to the previous item (space separated)
      if len(r_search)>0:
        r_search[len(r_search)-1].append(r)

  if r_search_invalid:
    e = 'error: invalid search item'
    sys.stderr.write('{}\n'.format(
        '\n'.join([ '{}: {}'.format(e,i) for i in r_search_invalid ])
    ))
  else:
    # insert items for polish notation
    [ r_search.insert(0, p) for p in r_addr ]

    # join multi part items
    r_search = [ ' '.join(e) for e in r_search ]

    # "explode" into imap search
    r_search = ' '.join([ '{} "{}"'.format(
        e.split(c.r_search_token)[0].upper(),
        e.split(c.r_search_token)[1])
        for e in r_search
    ])

    # apply polish (and/)or notation for "addr"
    r_search = '{}{}'.format('OR '*(len(r_addr)-1), r_search)
    ret = r_search

  return ret


def parse_config(): #---------------------------------------------------------
  if os.path.exists(c.fn_config):
    cf = configparser.RawConfigParser()
    cf.read(c.fn_config)
    kv = {
        "global": [
          "#console_width",
          "date_format",
          "html_cmd",
          "html_charset",
          "locale",
          "&use_pager",
        ],
        "view": [
          "attachment_info",
          "display_headers",
          "&auto_mark",
        ],
        "list": [
          "#address_length",
          "#messages",
          "#subject_length",
        ]}
    for k,v in kv.items():
      for v in v:
        try:
          if v[0] == "#":
            v = v[1:]
            setattr(c, 'opt_{}'.format(v), cf.getint(k, v))
          elif v[0] == "&":
            v = v[1:]
            setattr(c, 'opt_{}'.format(v), cf.getboolean(k, v))
          else:
            setattr(c, 'opt_{}'.format(v), cf.get(k, v))
        except:
          pass


def parse_accounts_file(): #--------------------------------------------------
  """reads accounts from config"""

  if not os.path.exists(c.fn_accounts):
    sys.stderr.write("please provide an `accounts' file\n")
  else:
    st = os.stat(c.fn_accounts)
    if st.st_mode != 33152 or \
        st.st_uid != os.geteuid() or \
        st.st_gid != os.getegid():
      lecture = """
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @          WARNING: UNPROTECTED ACCOUNTS FILE!            @
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        Permissions {} for '{}' are too open.
        It is required that your accounts file is NOT accessible by others.
        Possible actions:
        - chmod 0600 {}
        - chown {}:{} {}
         accounts file will be ignored.
        Load accounts file "{}": bad permissions
      """.strip()
      lecture = os.linesep.join(
          [ l.strip() for l in lecture.split(os.linesep) ])
      lecture = lecture.format(
          oct(st.st_mode)[-4:], c.fn_accounts,
        c.fn_accounts,
        os.geteuid(), os.getegid(), c.fn_accounts,
        c.fn_accounts
      )
      sys.stderr.write("{}\n".format(lecture))
      sys.exit(1)

    with open(c.fn_accounts) as f:
      idx = 0
      for line in f.readlines():
        line = line.strip()
        if line[0] != '#':
          # non-comment lines contain account specification
          # -> e-mail, server, base64[username, passwd]
          idx += 1
          x = Account(line)
          if x.addr:
            x.id = idx
            c.accounts[x.addr] = x

    c.accounts = sorted(c.accounts.items(), key=lambda x:x[1].id)


def list_accounts(): #--------------------------------------------------------
  _max = len(max([a for a,x in c.accounts], key=len))
  for k,v in c.accounts:
    l = "{}{} {}: {}".format(
        ' *'[c.opt_account_id == v.id],
        str(v.id).ljust(2), v.addr.ljust(_max), v.host)
    print(l)


def get_active_account(): #---------------------------------------------------
  ret = (None, None)

  if type(c.opt_account_id) == str:
    if c.opt_account_id.isdigit():
      c.opt_account_id = int(c.opt_account_id)
    else:
      # try one which contains
      if c.accounts:
        i = [i+1 for i, a in enumerate(c.accounts)
            if a[0].lower().find(c.opt_account_id.lower())>-1]
        if len(i) == 1:
          c.opt_account_id = i[0]
      if type(c.opt_account_id) != int:
        c.opt_account_id = -1

  if type(c.opt_account_id) == int:
    if c.opt_account_id < 1 or c.opt_account_id > len(c.accounts):
      c.opt_account_id = -1
    else:
      try:
        open(c.fn_active, 'w').write(str(c.opt_account_id))
      except:
        pass

  if not c.opt_account_id:
    stored_id = None
    # do we an previous store id
    try:
      stored_id = int(open(c.fn_active, 'r').read().strip())
    except:
      pass
    if stored_id:
      if stored_id > 0 and stored_id <= len(c.accounts):
        c.opt_account_id = stored_id

  if c.opt_account_id:
    if c.opt_account_id < 1:
      c.opt_account_id = None
    else:
      ret = c.accounts[c.opt_account_id-1]

  return ret


# Helper functions ===========================================================
def truncate(s, l): #---------------------------------------------------------
  ret = s
  if l:
    l = int(l)
    ret = (ret[:l-2] + '..') if len(ret) > l else ret
  return ret

def decode(s): #--------------------------------------------------------------
  return (s.decode('utf-8') if hasattr(s,'decode') else s)

def email_from_address_list(s): #---------------------------------------------
  ret = re.findall("([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", s)
  # uniq addresses; fixes for entries like: "a@b.c" <a@.b.c>
  ret = list(set(ret))
  if ret:
    ret = '{}{}'.format(('+' if len(ret)>1 else ''), ret[0])
  else:
    ret = s.split('<')[0]
  return ret.lower()

def email_localize_date(date_str): #------------------------------------------
  ret = datetime.fromtimestamp(
      email.utils.parsedate_to_datetime(date_str).timestamp()
      ).strftime(c.opt_date_format)
  return ret

def shrinkuser(path): #-------------------------------------------------------
  """shrink environment variable HOME to '~'"""
  return ('~{}'.format(path.split(os.environ['HOME'],1)[1]) if
    'HOME' in os.environ else path)


# Views ======================================================================
def view_redirect_stdout(): #-------------------------------------------------
  import subprocess # Subprocesses with accessible I/O streams

  # shell command with arguments
  cmd="less -F -K -R -S -X".split()
  # -F quit if content fits on screen
  # -K quit on ctrl-C
  # -R proces ANSI "color" escape sequences
  # -S truncated long lines instead of wrapping them
  # -X disable termcap initialization (like prevent clear screen)

  try:
    # initialize pager process
    proc = subprocess.Popen(
        # command to execute
        cmd,
        # pass over user's environment
        env=os.environ,
        # assign stdin/stdout
        stdin=subprocess.PIPE, stdout=sys.stdout,
        # decode stdin, stdout and stderr
        universal_newlines=True,
    )
    sys.stdout = proc.stdin
    # keep track
    c.proc_pager = proc
  except: # didn't work, no harm done
    pass


def view_message_handle_html(data): #-----------------------------------------
  import subprocess # Subprocesses with accessible I/O streams

  use_sys_stderr = c.opt_html_cmd == None

  # shell command with arguments
  if not use_sys_stderr:
    cmd = c.opt_html_cmd.split()
    try:
      # initialize pager process
      proc = subprocess.Popen(
          # command to execute
          cmd,
          # pass over user's environment
          env=os.environ,
          # assign stdin/stdout
          stdin=subprocess.PIPE, stdout=subprocess.PIPE,
      )

      # write text/html to process
      proc.stdin.write(data.encode('utf-8'))
      # get the output
      output = proc.communicate()[0]
      for line in output.split(os.linesep.encode('utf-8')):
        print(line.decode(c.opt_html_charset, 'replace'))
      proc.terminate()
    except:
      use_sys_stderr = True

  if use_sys_stderr:
    # if we use pager, feed the html data to the pager stream,
    # otherwise if we redirect the stderr, write it there
    if sys.stderr.isatty():
      print(data)
    else:
      sys.stderr.write(data)
      print()


def view_list_messages(imap, msgids, address): #------------------------------
  status = imap.status()
  print("{}: {} ".format(address,decode(status).lower()))
  if msgids:
    messages = []
    max_address_length = 0
    max_messages = c.opt_messages
    addr_header = 'From'
    for msgid in msgids[:max_messages]:
      message = imap.fetch(msgid)
      messages.append(message)
      addr = email_from_address_list(message.get(addr_header))
      if addr == address:
        addr_header = 'To'
        addr = email_from_address_list(message.get(addr_header))
      if len(addr) > max_address_length:
        max_address_length = len(addr)
    if c.opt_address_length != 0 \
        and max_address_length > c.opt_address_length:
      max_address_length = c.opt_address_length
    for message in messages:
      addr = email_from_address_list(message.get(addr_header))
      line = "{:<{l}}  ".format(
          message.get('Msgid').split('/')[-1], l=len(str(msgids[0])))
      line = "{}{}  ".format(line,
          email_localize_date(message.get('Date')))
      line = "{}{:<{l}}  ".format(line,
          truncate(
            addr,
            max_address_length),
          l = max_address_length)
      if not 'seen' in message.get('Flags').lower().split():
        line = "{}* ".format(line)
      elif 'answered' in message.get('Flags').lower().split():
        line = "{}> ".format(line)
      else:
        line = "{}  ".format(line)
      max_subject_length = int(c.opt_console_width) - len(line) - 1
      if c.opt_subject_length != 0:
        max_subject_length = c.opt_subject_length
      if max_subject_length < 0:
        max_subject_length = None
      line = "{}{}".format(line,
          truncate(message.get('Subject').replace("\r\n",""),
            max_subject_length))
      print(truncate(line, c.opt_console_width))


def view_list_search(imap, address, search): #--------------------------------
    data = imap.search(search)
    if data:
      msgids = [ decode(e) for e in data ]
      view_list_messages(imap, msgids, address)


def view_message(imap, msgid): #----------------------------------------------

  message = imap.fetch(msgid, full=True)
  if not message:
    sys.stderr.write("message not found...\n")
    sys.exit(1)
  else:
    message.email_localize_date = email_localize_date
    body = message.body()
    # print display headers
    print(message.headers(c.opt_display_headers))
    # add list of attechments
    attachments = message.attachments(info=c.opt_attachment_info)
    if attachments:
      print(attachments)
    # print the text/plain message body
    print()
    if not "plain" in body or \
        (c.opt_prefer_html and "html" in body):
      # using stderr, one can do something like:
      # dmc msgid 2> >(elinks --force-html --dump)
      view_message_handle_html(body['html'])
    else:
      print(body['plain'])

    if c.opt_auto_mark:
      # mark as seen
      flags = message.get('Flags').lower().split()
      if not 'seen' in flags:
        imap.readonly = False
        imap.select(c.r_folder)
        imap.store(msgid, '+FLAGS', r'(\Seen)')


def view_list_folders(imap, address, folder=None): #--------------------------
  folders = imap.list()
  folders = sorted(folders, key=lambda x:x)
  folders = [ '{}{}'.format(n, ('*' if 'marked' in f.split() else ' '))
      for n,f in folders ]
  folders = [
      '  {}{}'.format('  '*(len(f.split('/'))-1), f)
      for f in folders ]
  print('{}: folders (count {} marked {})'.format(
    address,
    len([f for f in folders]),
    len([f for f in folders if f.endswith('*')]),
  ))
  print(os.linesep.join(folders))


def message_toggle_seen(imap, msgid): #---------------------------------------
  message = imap.fetch(msgid)
  if not message:
    sys.stderr.write("message not found...\n")
    sys.exit(1)

  imap.readonly = False
  imap.select(c.r_folder)

  flags = message.get('Flags').lower().split()
  imap.store(msgid, '{}FLAGS'.format(
    ('-' if 'seen' in flags else '+')),
    r'(\Seen)')


def message_delete(imap, address, msgid): #-----------------------------------
  message = imap.fetch(msgid)
  if not message:
    sys.stderr.write("message not found...\n")
    sys.exit(1)

  view_list_messages(imap, [msgid], address)

  if c.proc_pager:
    c.proc_pager.stdin.close()
    c.proc_pager.wait()
    c.proc_pager.terminate()
    sys.stdin = sys.__stdin__
    sys.stdout = sys.__stdout__

  answer = input("delete [y/N]? ")
  if answer and answer in 'Yy':
    imap.readonly = False
    imap.select(c.r_folder)
    imap.store(msgid, '+FLAGS', r'(\Deleted)')
    imap.expunge()


# ============================================================================
# ============================================================================
# ============================================================================
if __name__ == "__main__":

  # main configuration
  c = Config()

  # handle given arguments
  try:
    parse_config()
    parse_arguments()
    parse_accounts_file()

    # if there are search key arguments, parse them..
    if c.r_search:
      c.r_search = parse_search_arguments(c.r_search)
      if c.r_search == None:
        # ..to quit before doing something else
        sys.exit(1)

    # check shorthand action (if set)
    if c.r_shorthand:
      # shorthands:
      # u = list unseen messages
      if not c.r_shorthand in list('dmtu'):
        sys.stderr.write('error: unknown shorthand: {}\n'.format(
          c.r_shorthand))
        sys.exit(1)

    address, account = get_active_account()
    if not c.opt_account_id or c.opt_list_accounts:
      list_accounts()
      sys.exit(1 * (0 if c.opt_list_accounts else 1 ))

    # set global locale
    locale.setlocale(locale.LC_ALL, c.locale)

    if c.opt_messages == 0:
      c.opt_messages = c.opt_console_height

    # enable redirect to less-pager
    if c.opt_use_pager:
      view_redirect_stdout()

    imap = imapclient()

    if c.opt_debug > 2:
      imap.debug(c.opt_debug)

    imap.connect(account.host, account.addr, account.pwrd)
    if imap.isconnected():
      if c.opt_list_folders:
        view_list_folders(imap, address)
      else:
        msgid = imap.select(c.r_folder)
        if c.r_shorthand:
          # handle shorthand actions
          if c.r_shorthand == 'd':
            message_delete(imap, address, (c.r_msgid if c.r_msgid else msgid))
          elif c.r_shorthand == 'm':
            data = imap.search('UNSEEN')
            if data:
              msgids = [decode(e) for e in data]
              view_message(imap, msgids[-1])
            else:
              view_message(imap, msgid)
          elif c.r_shorthand == 'u':
            view_list_search(imap, address, 'UNSEEN')
          elif c.r_shorthand == 't':
            message_toggle_seen(imap, (c.r_msgid if c.r_msgid else msgid))
        elif c.r_msgid:
          view_message(imap, c.r_msgid)
        elif c.r_search:
          view_list_search(imap, address, c.r_search)
        else:
          if msgid:
            msgid = int(msgid)
            msgids = list(range(msgid, msgid-c.opt_messages, -1))
            # filter msgid < 1
            msgids = [ i for i in msgids if i > 0 ]
            view_list_messages(imap, msgids, address)
    imap.disconnect()

  except KeyboardInterrupt:
    pass

  # if pager is active, wait for it to finish
  if c.proc_pager:
    c.proc_pager.stdin.close()
    c.proc_pager.wait()
    c.proc_pager.terminate()

  sys.exit(0)
